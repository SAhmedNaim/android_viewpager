package com.sahmednaim.viewpager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by DIU on 31-Jul-18.
 */

public class FragmentOne extends Fragment
{
    View view;
    Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_one_layout, container, false);


        button = (Button) view.findViewById(R.id.my_site_button);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Clicked On Button", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
}
