package com.sahmednaim.viewpager;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        setUpMyViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
    }

    void setUpMyViewPager(ViewPager myViewPager)
    {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addMyFragment(new FragmentOne(), "Naim");
        viewPagerAdapter.addMyFragment(new FragmentTwo(), "Nurul");
        viewPagerAdapter.addMyFragment(new FragmentThree(), "Nobin");

        myViewPager.setAdapter(viewPagerAdapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> fragments = new ArrayList<Fragment>();
        private final List<String> strings = new ArrayList<String>();

        public ViewPagerAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return fragments.get(position);
        }

        @Override
        public int getCount()
        {
            return fragments.size();
        }

        void addMyFragment(Fragment fragment, String title)
        {
            fragments.add(fragment);
            strings.add(title);
        }

        public CharSequence getPageTitle(int position)
        {
            return strings.get(position);
        }

    }

}
